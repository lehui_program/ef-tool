# Changelog

## [v1.0.5] 2024-01

### 🐞Bug修复

* 优化RSA中文加密后解密乱码问题

## [v1.0.4] 2024-01

### 🐣新特性

* 新增RSA加密,提供了生成密钥,加解密,签名,验签等方法

## [v1.0.3] 2024-01

### 🐣新特性

* 新增ArrayUtil中的removeEmptyValues,remove,,union,zip,unzip等方法
* 新增StrUtil中的camelCase,capitalize,truncate,toUpper,toLower等方法
* 新增DateUtil中的dateDiff,strDateDiff方法

### 🐞Bug修复

* 暂无

## [v1.0.2] 2024-01

### 🐣新特性

* 新增ArrayUtil中的setOrAppend,replace,clone,filter,reverse等方法
* 新增CharUtil中的isAscii,isEmoji等方法
* 新增IdCardUtil中的getProvinceCodeByIdCard，getCityCodeByIdCard等方法

### 🐞Bug修复

* 暂无

## [v1.0.1] 2024-01

### 🐣新特性

* 新增调用后台方法进行分页工具类PageQuery

### 🐞Bug修复

* 优化常量中的注释
* 优化util中的方法
* 优化PageUtil中的默认每页记录数为10条
* 优化PageUtil中的计算方法

## [v1.0.0] 2024-01

### 🐣新特性

* 发布正式版本

### 🐞Bug修复

* 暂无